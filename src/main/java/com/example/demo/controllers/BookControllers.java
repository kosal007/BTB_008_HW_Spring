package com.example.demo.controllers;

import com.example.demo.models.Book;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookControllers {
    BookService bookService;
    @Autowired
    public BookControllers(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/index")
    public String index(Model model){
        List<Book>bookList =this.bookService.getBookList();
        model.addAttribute("books",bookList);
        return "index";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable Integer id,Model model)
    {
        Book book=this.bookService.findId(id);
        model.addAttribute("book",book);
        System.out.println("ID: "+id);
        return "views";
    }
    @GetMapping("/update/{book_id}")
    public String update(@PathVariable Integer book_id,Model model)
    {
        Book book=this.bookService.findId(book_id);
        model.addAttribute("book",book);
        return "update";
    }
    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file)
    {
        if(file==null){
            return null;
        }
        File path =new File("/btb1");
        if (!path.exists())
            path.mkdir();
        String filename =file.getOriginalFilename();
        String extension =filename.substring(filename.lastIndexOf('.')+1);
        System.out.println(filename);
        System.out.println(extension);
        filename=UUID.randomUUID()+"."+extension;
        try{
            Files.copy(file.getInputStream(),Paths.get("/btb1",filename));
        }catch (IOException e){}
        book.setThumbnail("/imgs/"+filename);
        this.bookService.update(book);
      return "redirect:/index";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id)
    {
        this.bookService.delete(id);
        return "redirect:/index";
    }
    @GetMapping("/create")
    public String create(Model model)
    {
        model.addAttribute("book",new Book());
        return "create-book";
    }
    @PostMapping("create/submit")
    public String cteate(@Valid Book book, BindingResult bindingResult, MultipartFile file){
        if(file==null){
            return null;
        }
        File path =new File("/btb1");
        if (!path.exists())
            path.mkdir();
            String filename =file.getOriginalFilename();
            String extension =filename.substring(filename.lastIndexOf('.')+1);
            System.out.println(filename);
            System.out.println(extension);
            filename=UUID.randomUUID()+"."+extension;
            try{
                Files.copy(file.getInputStream(),Paths.get("/btb1",filename));
            }catch (IOException e){}
            book.setThumbnail("/imgs/"+filename);
        if(bindingResult.hasErrors())
        {
            return "create-book";
        }

        System.out.println(book);
        this.bookService.create(book);
        return "redirect:/index";
    }
}
