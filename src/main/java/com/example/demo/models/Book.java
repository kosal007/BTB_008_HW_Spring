package com.example.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Book {
    private String thumbnail;
    @NotNull
    private Integer id;
    @Size(min = 5,max = 255,message = "min and max")
    @NotNull
    private String title;
    private String author;
    private String publisher;
    public Book(){}

    public Book(Integer id, String title, String author, String publisher,String thumbnail) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.thumbnail=thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Book{" +
                "thumbnail='" + thumbnail + '\'' +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
