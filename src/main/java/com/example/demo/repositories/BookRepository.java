package com.example.demo.repositories;


import com.example.demo.models.Book;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
//Repository Create Bean in container
@Repository
public class BookRepository {
    Faker faker =new Faker();
   List<Book> bookList=new ArrayList<>();
    {
        for (int i=0;i<10;i++)
        {
            Book book =new Book();
            book.setId(i);
            book.setTitle(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());
            bookList.add(book);
        }
    }
    //Get for view
    public List<Book> getBookList() {
        return this.bookList;
    }
    public Book findId(Integer id)
    {
        for (int i=0;i<bookList.size();i++)
        {
            if (bookList.get(i).getId()==id)
            {
                return bookList.get(i);
            }
        }
        return null;
    }
    public boolean update(Book book)
    {
       for (int i=0; i<bookList.size();i++)
       {
          if (bookList.get(i).getId()==book.getId())
          {
              bookList.set(i,book);
              return true;
          }
       }
        return false;
    }
    public boolean delete(Integer id)
    {
    for (int i=0;i<bookList.size();i++)
    {
        if (bookList.get(i).getId()==id)
        {
            bookList.remove(i);
            return true;
        }
    }
    return false;
    }
    public boolean create(Book book)
    {
     return bookList.add(book);
    }
}
