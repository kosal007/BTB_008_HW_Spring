package com.example.demo.services.impl;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImplement implements BookService {
    private BookRepository bookRepository;
    @Autowired
    public BookServiceImplement(BookRepository bookRepository){
        this.bookRepository=bookRepository;
    }
    @Override
    public List<Book>getBookList(){
        return this.bookRepository.getBookList();
    }
    @Override
    public Book findId(Integer id){
        return this.bookRepository.findId(id);
    }
    @Override
    public boolean update(Book book){
        return this.bookRepository.update(book);
    };

    @Override
    public boolean delete(Integer id) {
        return this.bookRepository.delete(id);
    }
    @Override
    public boolean create(Book book){return this.bookRepository.create(book);}
}
